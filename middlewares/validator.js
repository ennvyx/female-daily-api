const response = require("../components/response");
const { body, param, query, validationResult } = require("express-validator");

exports.getFetchUsers = () => {
  return [
    query("page").exists().withMessage("Missing [query] page request"), 

  ];
};

exports.getUsers = () => {
  return [

  ];
};

exports.getUserDetail = () => {
  return [
    param("id").exists().withMessage("Missing [param] id request")
  ];
};


exports.postCreateUser = () => {
  return [
    body("id").exists().withMessage("Missing [body] id request"),
    body("email").exists().withMessage("Missing [email] id request"),
    body("firstName").exists().withMessage("Missing [body] firstName request"),
    body("lastName").exists().withMessage("Missing [body] lastName request"),
    body("avatar").exists().withMessage("Missing [body] avatar request")
  ];
};

exports.putUpdateUser = () => {
  return [
    query("id").exists().withMessage("Missing [query] id request"),
    body("email").exists().withMessage("Missing [email] id request"),
    body("firstName").exists().withMessage("Missing [body] firstName request"),
    body("lastName").exists().withMessage("Missing [body] lastName request"),
    body("avatar").exists().withMessage("Missing [body] avatar request")
  ];
};

exports.deleteUser = () => {
  return [
    query("id").exists().withMessage("Missing [query] id request"),
  ];
};

exports.validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return response.res400(res,errors);

};
