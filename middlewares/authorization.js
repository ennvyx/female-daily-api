const response = require("../components/response");
const serverApiKey = process.env.X_API_KEY;
const serverDeleteAuthorization = process.env.DELETE_AUTHORIZATION;

exports.validate = async (req, res, next) => { 
    const reqApiKey = req.headers['x-api-key'];
    if (!req.headers['x-api-key']) {
      return response.res401(res, '!apiKey')
    }
    if (req.method === "DELETE") {
      if (req.headers.authorization && req.headers.authorization != serverDeleteAuthorization) {

        return response.res401(res, 'invalidDeleteAuth');
      }
      else if(!req.headers.authorization) {
        return response.res401(res,'!deleteAuth');
      }
    }
    
    try {
      if (reqApiKey === serverApiKey) {
        next();
      }
      else {
        return response.res401(res,'invalidApiKey')
      }
    }
    catch(err) {
        console.log("authorization validate error : ", err);
        return response.res500(res)    
    }
    

  };


  