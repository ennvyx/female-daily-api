"use strict";

const {users,Op} = require("../components/database");
const response = require("../components/response");
const axios = require ("axios");
const utilities = require("../components/helper/utilities")
const moment = require("moment");
const fetchUrl = process.env.FETCH_URL;

exports.fetchUsers = async (req, res) => {

  try {
    const requestedPage = req.query.page; 
    const fetchedUsers = await axios.get(fetchUrl, {params: {page: requestedPage}}).then((result) => {return result.data.data});

    if (fetchedUsers.length != 0 || fetchedUsers != undefined || fetchedUsers != null) {
      for (let i = 0 ; i != fetchedUsers.length ; i++) {
        const isExist = await utilities.existChecker(fetchedUsers[i].id);
        fetchedUsers[i].created_at = moment();
  
        if (isExist === false) {
          await users.create(fetchedUsers[i]);
        }
        else if (isExist != false && isExist.deleted_at != null || isExist.deleted_at != undefined) {
          await users.update({
            email: fetchedUsers[i].email,
            first_name: fetchedUsers[i].first_name,
            last_name: fetchedUsers[i].last_name,
            avatar: fetchedUsers[i].avatar,
            created_at: fetchedUsers[i].created_at,
            updated_at: null,
            deleted_at: null,
          },{
            where: {
              id: fetchedUsers[i].id
            }
          });
        }
      }
    }

    return response.res200(res,'Success Fetching Users Data', fetchedUsers);

  }
  catch(err) {
  
    console.log("User fetch error : ", err);
    return response.res500(res,'System error, please contact helpdesk / administrator', null);

  }

};

exports.getUsers = async (req, res) => {

    try { 

      const usersData = await users.findAll({
        where: {
          deleted_at: null
        },
        attributes: [
          'id',
          'email',
          ['first_name', 'firstName'],
          ['last_name', 'lastName'],
          'avatar'
        ],
        order: [
          ['id','ASC']
        ],
        raw: true
      });

      return response.res200(res,'Success', usersData);



    }
    catch(err) {
    
      console.log("Get Users error: ", err);
      return response.res500(res,'System error, please contact helpdesk / administrator', null);
  
    }
  
  };

exports.userDetail = async (req, res) => {

  try {
   const requestedUserId = req.params.id;
   const userData = await users.findOne({
    where: {
      id: requestedUserId,
      deleted_at: null,
    },
    attributes: [
      'id',
      'email',
      ['first_name', 'firstName'],
      ['last_name', 'lastName'],
      'avatar'
    ],
    raw: true
   });

   if (userData) {
    return response.res200(res,'Success', userData);

   }

   else {
    return response.res404(res,'!user');
   }
  }

  catch(err) {
  
    console.log("User Detail error: ", err);
    return response.res500(res,'System error, please contact helpdesk / administrator', null);

  }
};


exports.createUser = async (req, res) => {

  try {

   const isExist = await utilities.existChecker(req.body.id);
   
   if (isExist === false) { 
    await users.create({
      id: req.body.id,
      email: req.body.email,
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      avatar: req.body.avatar,
      created_at: moment()
     })
  
     return response.res200(res,'Success');
   }
   else if (isExist != false && isExist.deleted_at != null || isExist.deleted_at != undefined) {
    await users.update({
      email: req.body.email,
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      avatar: req.body.avatar,
      created_at: moment(),
      updated_at: null,
      deleted_at: null,
    },
    {
      where: {
        id: req.body.id
      }
    });
    return response.res200(res,'Success');

  }
  
   else if(isExist && isExist.deleted_at === null) {
  
    return response.res409(res,'userExist');
   }
   
  }

  catch(err) {
  
    console.log("Create user error: ", err);
    return response.res500(res,'System error, please contact helpdesk / administrator', null);

  }
};

exports.updateUser = async (req, res) => {

  try {
    await users.update({
      email: req.body.email,
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      avatar: req.body.avatar,
      updated_at: moment()
    },{
      where: {
        id: req.query.id
      }
    });

    return response.res200(res,'Success');
      
  }

  catch(err) {
  
    console.log("Update user error: ", err);
    return response.res500(res,'System error, please contact helpdesk / administrator', null);

  }
};

exports.deleteUser = async (req, res) => {

  try {
    await users.update({
      deleted_at: moment()
    },
    {
      where: {
        id: req.query.id
      }
    });

    return response.res200(res,'Success');
      
  }

  catch(err) {
  
    console.log("Update user error: ", err);
    return response.res500(res,'System error, please contact helpdesk / administrator', null);

  }
};

