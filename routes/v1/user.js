const apicache = require("apicache");
const cache = apicache.middleware;
const response = require("../../components/response");
const validator = require("../../middlewares/validator");
const express = require("express");
const router = express.Router();

const userController = require("../../controllers/user");
const authorization = require("../../middlewares/authorization");


const index = function (req, res, next) {
  response.res404(res,'!url');
};

router
.route("/")
.get(authorization.validate,validator.getUsers(), validator.validate,(req, res) => {
    userController.getUsers(req, res);
});

router
.route("/fetch")
.get(authorization.validate,validator.getFetchUsers(), validator.validate,(req, res) => {
    userController.fetchUsers(req, res);
});

router
.route("/:id")
.get(authorization.validate,validator.getUserDetail(), validator.validate,(req, res) => {
    userController.userDetail(req, res);
});

router
.route("/")
.post(authorization.validate,validator.postCreateUser(), validator.validate,(req, res) => {
    userController.createUser(req, res);
});

router
.route("/")
.put(authorization.validate,validator.putUpdateUser(), validator.validate,(req, res) => {
    userController.updateUser(req, res);
});

router
.route("/")
.delete(authorization.validate,validator.deleteUser(), validator.validate,(req, res) => {
    userController.deleteUser(req, res);
});


router.all("*", index);

module.exports = router;
