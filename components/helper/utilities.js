const {users} = require("../database");


exports.existChecker = (id) => {
  return new Promise(async (resolve, reject) => {
      try{
      const data = await users.findOne({
        where: {
          id
        },
        attributes: ['id', 'deleted_at'],
        raw: true
      });

      if (data) {
        resolve(data)
      }
      else {
        resolve(false);
      }
    }
    catch(err) {
      console.log("exist checker error : ", err);
      reject(err)
    }
    });
  };
