const { Sequelize, DataTypes, Op } = require("sequelize");

const maindb = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
    logging: false,
    timezone: "+07:00",
  }
);

 const usersModel = require("../models/users");
 const users = usersModel(maindb,DataTypes);


 

module.exports = {
  users,
  Op,
  sequelize: maindb
};
