"use strict";

exports.res200 = function (res, msg ,values = '') {
    var data = {
        responseCode: 354200,
        responseMessage: msg,
        data: values,
    };

    res.status(200);
    res.json(data);
    res.end();

};

exports.res404 = function (res, err ,values = "") {


    const mapped = errorCodeMapping(err);

    var data = {
        responseCode: mapped.errorCode,
        responseMessage: mapped.errorMessage,
        data: values
    };

    res.status(404);
    res.json(data);
    res.end();

};

exports.res400 = function (
    res,
    err,
    // values = "Request error. Please read the API documentation."
    values = ""

) {
    // const mapped = errorCodeMapping(err);
    var data = {
        responseCode: 40000,
        responseMessage: err.errors,
        data: values
    };

    res.status(400);
    res.json(data);
    res.end();

};

exports.res401 = function (
    res,
    err,
    // values = "Unauthorized access."
    values = ""
) {

    const mapped = errorCodeMapping(err);

    var data = {
        responseCode: mapped.errorCode,
        responseMessage: mapped.errorMessage,
        data: values
    };

    res.status(401);
    res.json(data);
    res.end();

};

exports.res403 = function (
    res,
    err,
    values = ""
) {
    const mapped = errorCodeMapping(err);
    var data = {
        responseCode: mapped.errorCode,
        responseMessage: mapped.errorMessage,
        data: values
    };

    res.status(403);
    res.json(data);
    res.end();

};

exports.res500 = function (
    res,
    msg,
    values = "Internal system failure. Please contact system administrator"
) {
    var data = {
        responseCode: 42199,
        responseMessage: msg,
    };

    res.status(500);
    res.json(data);
    res.end();

};

exports.res409 = function (res, err ,values = "") {

    const mapped = errorCodeMapping(err);
    var data = {
        responseCode: mapped.errorCode,
        responseMessage: mapped.errorMessage,
        data: values
    };

    res.status(409);
    res.json(data);
    res.end();

};

const errorCodeMapping = (error) => {
    let mapped;

    switch(error) {
        case '!apiKey':
            mapped = {
                errorCode: 421412,
                errorMessage: 'Missing x-api-key headers !'
            }
            break;
        case 'invalidApiKey':
            mapped = {
                errorCode: 421413,
                errorMessage: 'Invalid API Key !'
            }
            break;
        case '!user':
            mapped = {
                errorCode: 421404,
                errorMessage: 'User not exist !'
            }
            break;    
        case 'userExist':
            mapped = {
                errorCode: 421409,
                errorMessage: 'User id is already exist !'
            }
            break;
        case 'invalidDeleteAuth':
            mapped = {
                errorCode: 42120,
                errorMessage: 'Invalid Delete Authorization !',
            }
            break;
        case '!deleteAuth':
            mapped = {
                errorCode: 42121,
                errorMessage: 'Missing Delete Authorization !'
            }
            break;
        case '!url':
            mapped = {
                errorCode: 42444,
                errorMessage: 'Resources not found'
            }
            break;        
          
    }
    return mapped;
    
};